from django.urls import path
from accounts.views import sign_up, user_login, user_logout


urlpatterns = [
    path("signup/", sign_up, name="signup"),
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout")
]
